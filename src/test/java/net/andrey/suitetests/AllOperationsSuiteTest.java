package net.andrey.suitetests;

import net.andrey.TodosE2ETest;
import net.andrey.TodosOperationsAtAllFilterTest;
import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Created by aalis_000 on 25.03.2016.
 */

@RunWith(Categories.class)
@Suite.SuiteClasses({TodosE2ETest.class, TodosOperationsAtAllFilterTest.class})
public class AllOperationsSuiteTest {
}
