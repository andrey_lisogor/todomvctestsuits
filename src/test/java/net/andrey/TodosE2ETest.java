package net.andrey;

import net.andrey.categories.Smoke;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static net.andrey.pages.TodoMVC.*;

/**
 * Created by aalis_000 on 25.03.2016.
 */
@Category(Smoke.class)
public class TodosE2ETest {

    @Test
    public void testTasksLifeCycle() {
        givenHelper(Filter.ALL);

        add("1");

        startEdit("1", "1 edited").pressTab();
        assertItemsLeft(1);

        //complete
        toogle("1 edited");
        assertTasks("1 edited");

        filterCompleted();
        startEdit("1 edited", "1 cancel edit").pressEscape();

        //reopen
        toogle("1 edited");
        assertNoTasks();

        filterActive();
        assertTasks("1 edited");
        add("2");
        assertTasks("1 edited", "2");

        delete("1 edited");
        assertTasks("2");

        //complete all
        toogleAll();
        assertNoTasks();

        filterAll();
        assertTasks("2");
        clearCompleted();
        assertNoTasks();

    }



}
